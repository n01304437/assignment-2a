﻿<%@ Page Title="Web Programming Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Web programming.aspx.cs" Inherits="Assignment_2a._Default" %>

<asp:Content ContentPlaceHolderID="Web_programming" runat="server">
    <div id="body">
       <!-- ########## Tricky Concept ##############--> 
    <div class="row">
            <div class="col-md-3">
                <h2>Tricky concept</h2>
                <div id="trick">
                <p>
                    My web application solutions are a perfect fit for every business, large or small. You can be confident that your site will grow with you.
                </p>
                </div>
            </div>

        <!-- ######################## My code #########################-->


            <div class="col-md-3">
                <h2>My Code</h2>
                <div id="my code">
                <html>
	<head>
	<title> My web Page</title>
	</head>
	<body>
        
		<h1>Welcome to my Website</h1>
		<h2>Two</h2>
		<h3>Three</h3>
		<h4>Four</h4>
		<h5>Five</h5>
		<h6>Six</h6>
		
		<p>Paragraph tag</p>

		<p>hello, how<em> are</em> you</p>
		<p>Here is some content. <br/> Thank you</p>
	
	</body>
</html></div>
            </div>
            <div class="col-md-3">
                <h2>Searched Code</h2>
                <div id="searched code">
                    <table border="1" style="font-family:Georgia, Garamond, Serif;color:blue;font-style:italic;">
                            <tr>
                            <th>Table Header</th><th>Table Header</th>
                            </tr>
                            <tr>
                            <td>Table cell 1</td><td>Table cell 2</td>
                            </tr>
                            <tr>
                            <td>Table cell 3</td><td>Table cell 4</td>
                            </tr>
                      </table>

                </div>
                
            </div>
            <div class="col-md-3">
                <h2>Helpful Links</h2>
                <div id="links">
                <ul>
                    <li>https://www.html.am/html-codes/"</li>
                    <li>http://web-source.net/html_codes_chart.htm#link</li>
                    <li>https://www.techopedia.com/definition/23898/web-programming</li>

                   <li><a href="https://www.html.am/html-codes/"></a></li>
                   <li><a href="http://web-source.net/html_codes_chart.htm#link"></a></li>
                    <li><a href="https://www.techopedia.com/definition/23898/web-programming"></a></li>

                </ul>
                    </div>
            </div>
        </div>
    </div>

    </asp:Content>
  
