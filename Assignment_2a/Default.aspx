﻿
<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Assignment_2a._Default" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <div id="body">
        <div class="jumbotron">
            <img src="E:\manpreet study\Web Application Development 1\asp_.net\Assignment_2a\hills.jpg" />
            <h1>Hi there, I'm Jane Doe</h1>
            <p class="lead">Web Developer, Designer, Manager.</p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h2>Definition</h2>
            <p>
                Study guides can be broad based to facilitate learning in a number of areas, or be resources that foster comprehension of literature, research topics, 
                history, and other subjects. General topics include study and testing strategies; reading, writing, classroom, and project management skills; as well 
                as techniques for learning as an adult, with disabilities, and online. Some will summarize chapters of novels or the important elements of the subject.
                Study guides for math and science often present problems (as in problem-based learning) and will offer techniques of resolution.
            </p>
           
        </div>
      
    </div>

</asp:Content>
