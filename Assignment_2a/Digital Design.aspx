﻿<%@ Page Title="Degital Design Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Digital Design.aspx.cs" Inherits="Assignment_2a._Default" %>

<asp:Content ContentPlaceHolderID="Digital_design" runat="server">
    <div id="body">
        
    <div class="row">
            <div class="col-md-3">
                <h2>Tricky concept</h2>
                <p>
                    My web application solutions are a perfect fit for every business, large or small. You can be confident that your site will grow with you.
                </p>
                
            </div>
            <div class="col-md-3">
                <h2>My Code</h2>
                <p>
                    Generate a spectrum of reports on the fly. Get to know your user demographic, witness the impact of SEO, or tabulate your online revenue.
                </p>
                
            </div>
            <div class="col-md-3">
                <h2>Searched Code</h2>
                <p>
                    With a data-driven site, your customers can interact with your business 24/7.
                </p>
                
            </div>
            <div class="col-md-3">
                <h2>Helpful Links</h2>
                <p>
                    With CSS and bootstrap responsive designs, your site will look great on any device.
                </p>
                
            </div>
        </div>
    </div>

    </asp:Content>
  
